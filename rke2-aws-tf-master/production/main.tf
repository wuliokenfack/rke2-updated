provider "aws" {
    region = "us-east-1"
}



module "rke2" {
    source = "./.."
    cluster_name = var.cluster_name
    ami = var.ami
    subnets = var.subnets
    vpc_id = var.vpc_id
}


module "agents" {
    name = "test"
    source = "../modules/agent-nodepool"
    cluster_data = module.rke2.cluster_data
    subnets =  ["subnet-737e307d","subnet-8d4f97bc", "subnet-76980c10", "subnet-6f198a4e", "subnet-3156c76e", "subnet-c8c7e385"]
    vpc_id = "vpc-bbf555c6"
    ami = "ami-09e67e426f25ce0d7"
}