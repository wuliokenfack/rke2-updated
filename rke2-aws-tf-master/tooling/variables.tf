variable "cluster_name" {
    type = string
    default = "PAAS-TOOLING"
}

variable "ami" {
    type = string
    default = "ami-09e67e426f25ce0d7"
}


variable "subnets" {
    type = list(string)
    default =  ["subnet-737e307d","subnet-8d4f97bc", "subnet-76980c10", "subnet-6f198a4e", "subnet-3156c76e", "subnet-c8c7e385"]
}

variable "vpc_id" {
    type = string
    default =  "vpc-bbf555c6"
}